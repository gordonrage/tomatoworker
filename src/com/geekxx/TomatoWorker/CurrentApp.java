package com.geekxx.TomatoWorker;

import android.app.Activity;
import android.app.Application;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.PowerManager;
import android.os.Process;
import com.geekxx.TomatoWorker.debug.CrashActivity;
import com.geekxx.TomatoWorker.debug.GD;

import java.util.LinkedList;

/**
 * @author daiyu
 */
public class CurrentApp extends Application {


    public CustomConstants constants;

    private PowerManager.WakeLock wakeLock;


    public PendingIntent pi_AlarmTimeOver;

    @Override
    public void onCreate() {
        super.onCreate();
        constants = new CustomConstants(this);


        Thread.UncaughtExceptionHandler uncaughtExceptionHandler = new Thread.UncaughtExceptionHandler() {
            @Override
            public void uncaughtException(Thread thread, final Throwable ex) {

                //  如果是App初始化错误，不能跳转到崩溃界面，否则死循环。
                Intent intent = new Intent(CurrentApp.this, CrashActivity.class);
                //  必须是新任务，否则没有反应
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra("exception", ex);
                //  跳转到崩溃信息界面
                startActivity(intent);
                ex.printStackTrace();
                GD.error(ex);
                Process.killProcess(Process.myPid());
            }
        };

        Thread.setDefaultUncaughtExceptionHandler(uncaughtExceptionHandler);
        // ****  ↑↑↑↑↑↑↑↑↑↑↑↑Over↑↑↑↑↑↑↑↑↑↑↑↑
    }


    /**
     * 当前应用运行的Activity， 主要是为了让应用能得到当前用户界面的Activity。
     * 而且能退出应用时全部销毁。
     */
    private LinkedList<Activity> activities = new LinkedList<Activity>();

    /**
     * 当Activity 被create的时候放进来
     *
     * @param activity 被创建的Activity
     */
    public void addActivity(Activity activity) {
        activities.add(activity);
        GD.i("当前栈数量:" + activities.size());
        echoAllActivity();
    }

    /**
     * 当某个Activity destroy的时候
     *
     * @param activity 被销毁的Activity
     */
    public void removeActivity(Activity activity) {
        activities.remove(activity);
        GD.i("当前栈数量:" + activities.size());
        echoAllActivity();
    }

    /**
     * Test an activity is  whether in the activities stack.
     *
     * @param cls Activity class
     * @return true if contains this activity
     */
    public boolean testActivityInStack(Class cls) {
        for (Activity activity : activities) {
            if (activity != null && activity.getClass() == cls) {
                return true;
            }
        }
        return false;
    }

    public Activity getActivity(Class clz) {
        for (Activity activity : activities) {
            if (activity != null && activity.getClass() == clz) {
                return activity;
            }
        }
        return null;
    }

    /**
     * @deprecated 发布时一定要关闭
     */
    @Deprecated
    private void echoAllActivity() {
        for (Activity activity : activities) {
            GD.i("当前栈：" + activity.getClass());
        }
    }


    /**
     * 退出应用
     */
    public void exit() {
        for (Activity activity : activities) {
            if (activity != null) {
                activity.finish();
            }
        }
        Process.killProcess(Process.myPid());
    }

    public void releaseCpuLock() {
        if (wakeLock != null) {
            wakeLock.release();
            wakeLock = null;
        }
    }

    public void acquireCpuLock() {
        if (wakeLock != null) {
            wakeLock.release();
        }
        PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
//        wakeLock = pm.newWakeLock(PowerManager.FULL_WAKE_LOCK |
//                PowerManager.ACQUIRE_CAUSES_WAKEUP |
//                PowerManager.ON_AFTER_RELEASE, getClass().getCanonicalName());
        wakeLock = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, getClass().getCanonicalName());
        wakeLock.acquire();
    }

    /**
     * 获取到Application类
     * http://stackoverflow.com/questions/3826905/singletons-vs-application-context-in-android
     *
     * @param context 本应用的一个context
     * @return CurrentApp对象
     */
    public static CurrentApp obtainApp(Context context) {
        return (CurrentApp) context.getApplicationContext();
    }

}