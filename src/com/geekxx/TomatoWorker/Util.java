package com.geekxx.TomatoWorker;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

public class Util {



	
	public static int dip2px(float dipValue, float density) {
        return (int) (dipValue * density + 0.5f);
    }

    

    public static int px2dip(float pxValue, float density) {
        return (int) (pxValue / density + 0.5f);
    }

    public static String formatTime(long time){
        final int min = (int) (time / 60000);
        final int seconds = (int) ((time % 60000) / 1000);
        return min + " : " + seconds;
    }
    

    /**
     * 检测网络状况
     */
    public static boolean isOnline(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo info = cm.getActiveNetworkInfo();
        return info != null && info.isAvailable();
    }


}
