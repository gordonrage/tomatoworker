package com.geekxx.TomatoWorker.main;

import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.media.AudioManager;
import android.net.Uri;
import android.os.Build;
import android.preference.PreferenceManager;
import android.support.v4.app.NotificationCompat;
import com.geekxx.TomatoWorker.CurrentApp;
import com.geekxx.TomatoWorker.R;
import com.geekxx.TomatoWorker.debug.GD;
import com.geekxx.TomatoWorker.settings.SettingsActivity;

/**
 * Author: Gordon
 * Date: 2014/6/16 16:15
 * Todo:
 */
public class AlarmUtil {

    public static void cancelAlarmTask(Context context) {
        CurrentApp app = CurrentApp.obtainApp(context);
        if (app.pi_AlarmTimeOver != null) {
            AlarmManager am = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
            am.cancel(app.pi_AlarmTimeOver);
        }
    }

    public static void startAlarm(Context context, int milliseconds) {
        CurrentApp app = CurrentApp.obtainApp(context);
        AlarmManager am = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);

        //构造广播发送Intent
        if (null == app.pi_AlarmTimeOver) {
            Intent intent = new Intent(AlarmReceiver.ALARM_RECEIVER_ACTION);
            app.pi_AlarmTimeOver = PendingIntent.getBroadcast(context,
                    111123949, intent, PendingIntent.FLAG_ONE_SHOT);
        }
        //设置时间触发器
        //到指定分钟之后 有个事件触发 pi 广播发送
        if (Build.VERSION.SDK_INT >= 19) {
            GD.i("Build.VERSION.SDK_INT >= 19");
            am.setExact(AlarmManager.RTC_WAKEUP, System.currentTimeMillis() + milliseconds, app.pi_AlarmTimeOver);
        } else {
            am.set(AlarmManager.RTC_WAKEUP, System.currentTimeMillis() + milliseconds, app.pi_AlarmTimeOver);
        }
        GD.i("start an alarm time:" + milliseconds);

    }

    public static void notifyTimeOver(Context context) {
        NotificationManager nm = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        NotificationCompat.Builder builder = new NotificationCompat.Builder(context);
        builder.setAutoCancel(true).setLights(Color.GREEN, 5000, 30000);

        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(context);
        if (pref.getBoolean(SettingsActivity.KEY_SOUND, true)) {
            //  mp3 file uri of the raw folder,
            builder.setSound(
                    Uri.parse("android.resource://" + context.getPackageName() + "/" + R.raw.on),
                    AudioManager.STREAM_NOTIFICATION
            );
        }
        if (pref.getBoolean(SettingsActivity.KEY_VIBRATE, false)) {
            builder.setVibrate(new long[]{0, 2000L});
        }
        Notification notification = builder.build();
        nm.notify(123123939, notification);
        GD.toast("Notification", context);
    }

}
