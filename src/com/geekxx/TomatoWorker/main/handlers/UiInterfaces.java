package com.geekxx.TomatoWorker.main.handlers;

/**
 * Author: Gordon
 * Date: 2014/6/16 13:22
 * Todo:
 */
public interface UiInterfaces {

    public interface OnCircleProgressViewChangedListener {
        public void onCircleProgressChanged(int progress);
    }

    public interface OnTimeOverListener {
        public void onTimeOver();
    }
}
