package com.geekxx.TomatoWorker.main;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.geekxx.TomatoWorker.CurrentApp;

/**
 * Author: Gordon
 * Date: 2014/6/16 16:12
 * Todo:
 */
public class AlarmReceiver extends BroadcastReceiver {

    public static final String PERMISSION_RECEIVER = "com.geekxx.TomatoWorker.permission.SendBroadcast";
    public static final String ALARM_RECEIVER_ACTION = "com.geekxx.TomatoWorker.actions.OnTimeOver";

    @Override
    public void onReceive(Context context, Intent intent) {
//        AlarmUtil.notifyTimeOver(context);
        MainActivity activity = (MainActivity) CurrentApp.obtainApp(context).getActivity(MainActivity.class);
        if (activity != null) {
            activity.onTimeOver();
        }
    }

}
