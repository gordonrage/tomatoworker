//package com.geekxx.TomatoWorker.main;
//
//
//import java.io.File;
//
//import android.app.Notification;
//import android.graphics.Color;
//import android.media.AudioManager;
//import android.net.Uri;
//
//public class NotificationBuilder {
//
//
//    /**
//     * 一个内部构建好的notification
//     */
//    private static Notification notification;
//
//
//    /**
//     * 重新创建Notification
//     */
//    public static void reBuildNotification() {
//        notification = new Notification();
//
//        //是否开启闪光模式
//        if (AppConfig.notification_is_LightMode) {
//            notification.flags = Notification.FLAG_SHOW_LIGHTS;
//            notification.ledARGB = Color.GREEN;
//            notification.ledOnMS = 5000;
//            notification.ledOffMS = 30000;
//        }
//
//        //是否开启铃声模式
//        if (AppConfig.notification_is_SoundMode) {
//            //设置声音通道，这里设置[通知]通道，不会占用媒体通道
//            notification.audioStreamType = AudioManager.STREAM_NOTIFICATION;
//            //设置声音
//            File audioFile = new File(AppConfig.notification_SoundPath);
////			File mp3File = new File("/mnt/sdcard2/audio/WP7/Ringtone 01.mp3");
//            if (audioFile.exists()) {
//                notification.sound = Uri.fromFile(audioFile);
//            } else {//如果声音文件不存在，那么使用默认
//                //先检测默认文件是否存在
//                if (App.self.defaut_mapFile.exists()) {
//                    notification.sound = Uri.fromFile(App.self.defaut_mapFile);
//                } else {
//                    //不存在，就使用系统铃声
//                    notification.defaults = Notification.DEFAULT_SOUND;
//                }
//            }
//        }
//
//        //是否开启振动模式
//        if (AppConfig.notification_is_VibrateMode) {
//            //vabrate 一个long类型的数组，第一个是延时，第二个是持续
//            //0秒延时 5秒钟持续
//            notification.vibrate = new long[]{0, 5000L};
//        }
//
//    }
//
//
//    /**
//     * 获取一个现成的一个Notification
//     */
//    public static Notification getNotification() {
//        if (notification == null) {
//            reBuildNotification();
//        }
//        return notification;
//    }
//}