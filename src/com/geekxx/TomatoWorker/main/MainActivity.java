package com.geekxx.TomatoWorker.main;

import android.animation.ObjectAnimator;
import android.app.Activity;
import android.app.NotificationManager;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.*;
import android.os.Process;
import android.preference.PreferenceManager;
import android.view.*;
import android.widget.Button;
import android.widget.Chronometer;
import android.widget.FrameLayout;
import android.widget.Toast;
import com.geekxx.TomatoWorker.CurrentApp;
import com.geekxx.TomatoWorker.R;
import com.geekxx.TomatoWorker.Util;
import com.geekxx.TomatoWorker.debug.GD;
import com.geekxx.TomatoWorker.main.handlers.UiInterfaces;
import com.geekxx.TomatoWorker.main.view.CircleProgressView;
import com.geekxx.TomatoWorker.settings.SettingsActivity;
import uk.co.ribot.easyadapter.ActivityViewHolder;
import uk.co.ribot.easyadapter.annotations.ViewId;

public class MainActivity extends Activity implements Chronometer.OnChronometerTickListener,

        UiInterfaces.OnTimeOverListener {

    ViewHolder holder;

    private int periodTime = 30 * 1000;

    //  max Progress
    final int maxProgress = 100;

    int totalSecond = periodTime / 1000;

    boolean hasStartedWorking;

    CountDownTimer countDownTimer;

    CurrentApp app;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        app = CurrentApp.obtainApp(this);
        app.addActivity(this);
        app.constants.initConstants(this);
        setContentView(R.layout.act_main);
        holder = new ViewHolder(this);

        //  2min  1s
        countDownTimer = new CountDownTimer(120000, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                GD.i("onTick : " + millisUntilFinished);
                holder.chronometer.setText(Util.formatTime(millisUntilFinished));
            }

            @Override
            public void onFinish() {
                //  callback
                MainActivity.this.onRestTimeOver();
            }
        };
    }

    @Override
    protected void onDestroy() {
        app.removeActivity(this);
        super.onDestroy();
    }

    private int tickedSeconds;
    private boolean ranTimeOver;

    @Override
    public void onChronometerTick(Chronometer chronometer) {
        holder.progressWheel.setSubCurProgress(maxProgress * tickedSeconds / totalSecond);
        if (tickedSeconds >= totalSecond) {
            onTimeOver();
        }
        tickedSeconds++;
    }

    @Override
    public void onTimeOver() {
        if (ranTimeOver) return;
        hasStartedWorking = false;
        AlarmUtil.notifyTimeOver(this);
        ranTimeOver = true;
        holder.chronometer.setText(R.string.s_start);
        holder.progressWheel.setSubCurProgress(0);
        holder.chronometer.stop();
        holder.hideCancelButton();
        onProcessRestTime();
    }

    /**
     * When user wanna cancel current task
     */
    public void onCancelTask() {
        hasStartedWorking = false;
        ranTimeOver = true;
        AlarmUtil.cancelAlarmTask(this);
        holder.chronometer.setText(R.string.s_start);
        holder.chronometer.stop();
        holder.progressWheel.setSubCurProgress(0);
        holder.hideCancelButton();
        app.releaseCpuLock();
    }


    /**
     * Trigger rest time.
     */
    public void onProcessRestTime() {
        //  prevent click event
        holder.chronometer.setEnabled(false);
        countDownTimer.start();

    }

    /**
     * Invoke when rest time over
     */
    public void onRestTimeOver() {
        holder.chronometer.setEnabled(true);
        holder.chronometer.setText(R.string.s_start);
        AlarmUtil.notifyTimeOver(MainActivity.this);
        app.releaseCpuLock();
    }


    @Override
    protected void onStop() {
        super.onStop();
        //  notification.
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        menu.add(0, R.id.menu_settings, 0, R.string.s_settings).setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
        menu.add(0, R.id.menu_cancel_notification, 0, R.string.s_cancel_notification);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_settings:
                startActivity(new Intent(this, SettingsActivity.class));
                break;
            case R.id.menu_cancel_notification:
                NotificationManager nm = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
                nm.cancelAll();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private long lastClickTime;

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            final long now = System.currentTimeMillis();
            if (lastClickTime + 2000 > now) {
                finish();
                android.os.Process.killProcess(Process.myPid());
            } else {
                Toast.makeText(this, getString(R.string.s_press_back_again_exit), Toast.LENGTH_SHORT).show();
            }
            lastClickTime = now;
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    private void onChronometerStart() {
        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(this);
        int work_minutes = pref.getInt(SettingsActivity.KEY_WORK_TIME, 10);
        totalSecond = work_minutes * 60;
        periodTime = totalSecond * 1000;  //  60 seconds per minute
        holder.progressWheel.setSubCurProgress(0);
        holder.showCancelButton();
        tickedSeconds = -1;
        ranTimeOver = false;
        AlarmUtil.startAlarm(this, periodTime);
        NotificationManager nm = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        nm.cancelAll();
        holder.chronometer.setBase(SystemClock.elapsedRealtime());
        holder.chronometer.start();
        app.acquireCpuLock();
        hasStartedWorking = true;
    }

    private static final class ViewHolder extends ActivityViewHolder implements View.OnClickListener {

        @ViewId(R.id.act_main__View_ProgressWheel)
        CircleProgressView progressWheel;

        @ViewId(R.id.act_main__Button_Cancel)
        Button btn_Cancel;

        @ViewId(R.id.act_main__Frame_Chronometer)
        FrameLayout frame_Chronometer;

        @ViewId(R.id.act_main__Chronometer)
        Chronometer chronometer;

        public ViewHolder(MainActivity activity) {
            super(activity);
            //  re layout
            CurrentApp app = CurrentApp.obtainApp(activity);
            ViewGroup.LayoutParams lp = frame_Chronometer.getLayoutParams();
            final int length = (int) (app.constants.device_w * 0.68);
            lp.height = length;
            lp.width = length;
            frame_Chronometer.setLayoutParams(lp);

            btn_Cancel.setOnClickListener(this);
            ObjectAnimator.ofFloat(btn_Cancel, "alpha", 1, 0).setDuration(100).start();
            chronometer.setOnChronometerTickListener(activity);
            chronometer.setOnClickListener(this);
            chronometer.setText(R.string.s_start);


            progressWheel.setWhetherDisplayPercent(false);
            progressWheel.getCircleAttribute().mSidePaintInterval = app.constants.dimens[12];

        }

        private void hideCancelButton() {
//            ObjectAnimator.ofFloat(btn_Cancel, "translationY", 0, btn_Cancel.getHeight()).setDuration(750).start();
            ObjectAnimator.ofFloat(btn_Cancel, "alpha", 1, 0).setDuration(750).start();

        }

        private void showCancelButton() {
//            ObjectAnimator.ofFloat(btn_Cancel, "translationY", 150, 0, -150, 0).setDuration(750).start();
            ObjectAnimator.ofFloat(btn_Cancel, "alpha", 0, 1).setDuration(750).start();
        }

        @Override
        public void onClick(View v) {
            final MainActivity activity = (MainActivity) getActivity();
            if (v == btn_Cancel) {
                activity.onCancelTask();
            } else if (v == chronometer) {
                if (!activity.hasStartedWorking) {
                    activity.onChronometerStart();
                }
            }
        }
    }


}
