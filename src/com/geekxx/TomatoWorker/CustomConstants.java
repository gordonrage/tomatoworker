package com.geekxx.TomatoWorker;

import android.app.Activity;
import android.content.res.Resources;
import android.os.Build;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import com.geekxx.TomatoWorker.main.UiUtil;

/**
 * Store some immutable values. treat as CONSTANT.
 */
public class CustomConstants {

    private boolean hasInitialed = false;


    public CustomConstants(CurrentApp app) {
        if (app.constants != null) {
            throw new RuntimeException("Only allow one instance of " + getClass().toString());
        }
    }

    /**
     * device width an height  value of pixels.
     */
    public int device_w, device_h;


    public int statusbarHeight;



    public float density;

    /**
     * dp对应的px， dimens[16] 对应 16dp 的 px值
     * by Gordon
     */
    public int[] dimens;


    /**
     * this handset device actionbar height.
     */
    public int actionbar_height;


    //判断SD是否还有空间，以决定图片是否缓存
//    public int fileSize;

    /**
     * 最好是在获取到density之后调用，而且只调用一次
     *
     * @param activity 因为要计算状态栏的高度，要Window，所以必须是Activity传入
     */
    public void initConstants(Activity activity) {
        if (hasInitialed) {
            throw new RuntimeException("Constants are only allowed initial once");
        }
        hasInitialed = true;
        final DisplayMetrics metrics = activity.getResources().getDisplayMetrics();

        density = metrics.density;
        device_h = metrics.heightPixels;
        device_w = metrics.widthPixels;



        dimens = new int[640];

        for (int i = 0; i < dimens.length; i++) {
            dimens[i] = Util.dip2px(i, density);
        }

        //  最新的设计都是48dp 默认
        actionbar_height = dimens[48];

        //  get actionbar height
        if (Build.VERSION.SDK_INT > 10) {
            TypedValue tv = new TypedValue();
            Resources.Theme theme = activity.getTheme();
            if (theme != null) {
                if (theme.resolveAttribute(android.R.attr.actionBarSize, tv, true)) {
                    actionbar_height = TypedValue.complexToDimensionPixelSize(tv.data, activity.getResources().getDisplayMetrics());
                }
            }
        }

        statusbarHeight = UiUtil.getStatusBarHeight(activity);

    }
}
