package com.geekxx.TomatoWorker.settings;

import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceManager;
import com.geekxx.TomatoWorker.CurrentApp;
import com.geekxx.TomatoWorker.R;
import com.geekxx.TomatoWorker.main.view.NumberPickDialog;

/**
 * Author: Gordon
 * Date: 13-12-25 下午9:10
 * Application setting activity
 */
public class SettingsActivity extends PreferenceActivity {


    /**
     * preference key of open sound
     */
    public final static String KEY_SOUND = "setting_open_sound";
    public final static String KEY_VIBRATE = "setting_open_vibrate";

    public final static String KEY_WORK_TIME = "setting_working_time";

    private CurrentApp app;


    @SuppressWarnings("deprecation")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        app = CurrentApp.obtainApp(this);

        addPreferencesFromResource(R.xml.setting);

        findPreference(KEY_WORK_TIME).setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(final Preference preference) {
                NumberPickDialog numberPickDialog = new NumberPickDialog(SettingsActivity.this, new NumberPickDialog.OnNumberPickOverListener() {
                    @Override
                    public void onGetNumber(int number) {
                        preference.getEditor().putInt(KEY_WORK_TIME, number).commit();
                    }
                });
                final int value = PreferenceManager.getDefaultSharedPreferences(SettingsActivity.this).getInt(KEY_WORK_TIME, 10);
                numberPickDialog.setDefaultValue(value);
                numberPickDialog.show();
                return false;
            }
        });

    }




//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        menu.add(R.string.s_exit).setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
//            @Override
//            public boolean onMenuItemClick(MenuItem item) {
//                // clear notification view
//                NotificationManager nm = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
//                nm.cancelAll();
//                app.exit();
//                return false;
//            }
//        }).setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
//        return super.onCreateOptionsMenu(menu);
//    }


}
